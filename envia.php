<?php require_once('conexion/conectar.php'); ?>

<?php
/* 		Author: Miguel Angel Gomez 		*/

$respuesta = "";

if(isset($_POST['fecha']) && ($_POST['numNoches']!="") && ($_POST['numPersonas']!="")){

	$fecha = $_POST['fecha'];
	$numNoches = $_POST['numNoches'];
	$numPersonas = $_POST['numPersonas'];

	$xmlRaw = file_get_contents("https://www.ruralgest.net/operadoresV9/modulos/buscadorV10/RuralGest_XML_V10.php?operador=43&fecha_e=${fecha}&n_noches=${numNoches}&n_personas=${numPersonas}&acceso=2&id_oficina=0");
	$dom = simplexml_load_string($xmlRaw);

	if($dom){

		$peti = $dom->info->id_peticion;
		$sistema = $dom->info->sistema;
		$compania = $dom->info->compania;
		$template = $dom->info->template;
		$fecha_e = $dom->busqueda->fecha_e;
		$n_noches = $dom->busqueda->n_noches;
		$n_personas = $dom->busqueda->n_personas;
		$operador = $dom->busqueda->operador;
		$id_oficina = $dom->busqueda->id_oficina;
		$acceso = $dom->busqueda->acceso;
		$tipo_aloja = $dom->busqueda->tipo_aloja;
		$provincia_casa = $dom->busqueda->provincia_casa;

		$agregoConsulta = "INSERT INTO consultas(`id_peticion`, `sistema`, `compania`, `template`, `fecha_e`, `num_noches`,
			`num_personas`, `operador`, `id_oficina`, `acceso`, `tipo_aloja`, `provincia_casa`) VALUES
			('$peti', '$sistema', '$compania', '$template', '$fecha_e', '$n_noches', '$n_personas', '$operador',
				'$id_oficina', '$acceso',	'$tipo_aloja', '$provincia_casa')";

		mysqli_select_db($conexion, $bd);
		$resultado = mysqli_query($conexion, $agregoConsulta) or die (mysqli_error($conexion));

		foreach($dom->alojamientos->alojamiento as $alojamiento){

			$importe = $alojamiento->seleccion->elemento->importe;
			$descrip = $alojamiento->descripcion;

			$consulta2 = "INSERT INTO casas VALUES ('$alojamiento->id_casa', '$alojamiento->nombre', '$importe')";
			mysqli_select_db($conexion, $bd);
			$resultado = mysqli_query($conexion, $consulta2) or die (mysqli_error($conexion));

			$agregoRelacion = "INSERT INTO historial_consultas (id_consulta, id_casa) VALUES('$peti', '$alojamiento->id_casa')";
			$resultado = mysqli_query($conexion, $agregoRelacion) or die (mysqli_error($conexion));

			$respuesta .= '
				<div class="post">
					<h2 class="title"><a href="#">Nombre: '.$alojamiento->nombre.'</a></h2>
					<p class="byline"> Id_interno: '.$alojamiento->id_casa.'</p>
					<div class="entry">
						<p>'.$descrip.'</p>
					</div>
					<div class="meta"><p class="links">Importe: '.$importe.'</p></div>
				</div>';
		}
	}
}
echo $respuesta;
?>
