<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--	AUTHOR: Miguel Angel Gomez 	--> 
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Prueba para RuralGest</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link rel="stylesheet" type="text/css" href="css/estilos.css" />
<link rel="stylesheet" type="text/css" href="css/jquery-ui.min.css">
<script type="text/javascript" src="js/jquery.js"></script>
<script src="js/jquery-ui.min.js" type="text/javascript"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>

<script type="text/javascript">
	function EnviarDatos(){
			var fecha       = document.getElementById('datepicker').value;
			var numNoches   = document.getElementById('numNoches').value;
			var numPersonas = document.getElementById('numPersonas').value;

			$.ajax({
					type:'POST',
					url:'envia.php',
					data:('fecha='+fecha+'&numNoches='+numNoches+'&numPersonas='+numPersonas),
					success:function(respuesta){
							$('#mio').html(respuesta);
					}
			})
	}
	</script>
</head>
<body>
<div id="wrapper">
	<div id="header">
		<div id="logo">
			<h1><a href="#">mini-ruralgest</a></h1>
			<h2><a>Pagina para prueba RURALGEST</a></h2>
		</div>
	</div>
	<div id="menu">
		<ul>
			<li class="active"><a href="#">Home</a></li>
			<li><a href="#">Products</a></li>
			<li><a href="#">About</a></li>
			<li><a href="#">Contact</a></li>
		</ul>
	</div>
	<div id="page"><div class="inner_copy"></div>
		<div id="page-bgtop">
			<div id="content">

				<div class="post">
					<h2 class="title"><a href="#">Busqueda alojamientos</a></h2>
					<p class="byline">Se obtiene XML remoto, se procesa en la BD interna y se muestra por pantalla</p>
					<div class="entry">
						<form method="post" action ="envia.php">
						<ul class="form-style-1">
								<li>
										<label>Numero de noches</label>
										<input type="text" name="numNoches" class="field" id="numNoches"/>
								</li>
								<li>
										<label>Numero de personas</label>
										<input type="text" name="numPersonas" class="field" id="numPersonas"/>
								</li>
								<li>
										<label>Fecha elegida:</label><br/>
										<input type="text" name="datepicker" id="datepicker" size="15" placeholder="llamada jquery">
										<script>		$("#datepicker").datepicker();			</script>
								</li>
								<li>
										<input type="button" value="Realizar pregunta" onclick="EnviarDatos()"/>
								</li>
						</ul>
						</form>
					</div>

				</div>
				<div id="mio">
				</div>
			</div>

			<div style="clear:both;"></div>
		</div>
	</div>
	<div id="footer"><div class="fleft"><p>Autor: Miguel Angel Gomez Baena.</p></div><div class="fright"></div><div class="fclear"></div></div>
</div>
</body>
</html>
